/*
 *  Höfundur: Heimir Þór Kjartansson
 *
 *  Þessari þáttarauppskrift má varpa í þáttara með BYACC/j á eftirfarandi hátt
 *      byaccj -J parser.y
 *  ef gert er ráð fyrir að byaccj sé skipun á kerfi notanda.
 *  Þessi þáttari er hannaður til að virka með lesgreini fengnum í gegnum JFlex 
 *  lesgreinasmið.
 *  Gert er ráð fyrir að JFlex lesgreinasmiðurinn búi til Java klasa Yylex sem 
 *  framkallaði þáttarinn getur talað við.
 *  Framkallaði þáttarinn er starfhæfur með fyrrnefndum lesgreini.
 *  Eiginleikar þáttara:
 *  * Tiltekur staðsetningu fundinna villna.
 *  * Hefur bæði gagnvirkt viðmót og les inn heilar skrár.
 *  * Skrifað er á aðalúttak öll gildi sem skilast úr fallsköllum svo hægt 
 *      einfalt sé að rekja virkni þáttara.
 *  Forkröfur þáttara:
 *  * Forrit þarf að hafa eitt fall main() sem meginfall án leppa.
*/
%{
    import java.io.*;
    import java.util.HashMap;
    import java.util.Scanner;
%}

%token <sval> TALA
%token <sval> LITERAL
%token <sval> OPERATOR
%token IF
%token ELSE
%token WHILE
%token VAR
%token <sval> NAME
%token RETURN

%%

Program                     :   { emit("\""+name+".mexe\" = main in\n!{{"); }
                                Element
                                { 
                                    emitLogicOperators();
                                    emit("}}*BASIS;"); 
                                }
                            ;
Element                     :   Function 
                            |   Function Element
                            ;
Function                    :   NAME '(' 
                                { 
                                    varCount = 0;
                                    varTable = new HashMap<String,Integer>();
                                } 
                                NamesOpt ')' 
                                {
                                    emit("#\"" + $1 + 
                                        "[f" + varCount + "]\" =");
                                    emit("[");
                                }
                                FunctionBody 
                                {
                                    emit("(Return)");
                                    emit("];");
                                }
                            ;
NamesOpt                    :   /* Nothing */
                            |   Names
                            ;
Names                       :   NAME {addVar($1);}
                            |   NAME {addVar($1);}',' Names
                            ;
FunctionBody                :   '{' VarListOpt StatementsOpt '}'
                            ;
VarListOpt                  :   /* Nothing */
                            |   VarList
                            ;
StatementsOpt               :   /* Nothing */
                            |   Statements
                            ;
Assignments                 :   AssignmentExpressions ';' ;
AssignmentExpressions       :   AssignmentExpression 
                            |   AssignmentExpression ',' AssignmentExpressions
                            ;
AssignmentExpression        :   NAME '=' Expression
                                {
                                    emit("(Store " + findVar($1) + ")");
                                }
                            ;
Statements                  :   Statement
                            |   Statement Statements
                            ;
Statement                   :   FlowControl
                            |   ExpressionHandler
                            |   Assignments
                            ;
VarList                     :   VAR Names ';';
FlowControl                 :   IF Expression 
                                {
                                    int falseLabel = newLabel();
                                    emit("(GoFalse _" + falseLabel + ")");
                                    push(falseLabel);
                                }
                                FlowBody 
                                {
                                    int trueLabel = newLabel();
                                    emit("(Go _" + trueLabel + ")");
                                    int falseLabel = pop();
                                    push(trueLabel);
                                    emit("_" + falseLabel + ":");
                                }
                                ElseOpt
                                {
                                    int trueLabel = pop();
                                    emit("_" + trueLabel + ":");
                                }
                            |   WHILE 
                                {
                                    int start = newLabel();
                                    emit("_" + start + ":");
                                    push(start);
                                }
                                Expression 
                                {
                                    int end = newLabel();
                                    emit("(GoFalse _" + end +")");
                                    push(end);
                                }
                                FlowBody
                                {
                                    int end = pop();
                                    int start = pop();
                                    emit("(Go _" + start + ")");
                                    emit("_" + end + ":");
                                }
                            ;
ElseOpt                     :   /* Nothing */
                            |   Else
                            ;
Else                        :   ELSE FlowBody;
ExpressionHandler           :   Expression ';'
                            |   RETURN Expression ';'
                                {
                                        /* DEBUG */
                                    emit("(StoreArgAcc " + 
                                        temporaryActivationRecord + " 0)");
                                     emit("(Call #\"writeln[f1]\" " +
                                        temporaryActivationRecord + ")"); 
                                        /* DEBUG */
                                    emit("(Return)");
                                }
                            ;
Expression                  :   PrimaryExpression
                            |   OPERATOR 
                                Expression 
                                {
                                    emit("(StoreArgAcc " + 
                                        --temporaryActivationRecord + " 0)");
                                    emit("(Call #\"" + $1 + "[f1]\"" +
                                    temporaryActivationRecord++ + ")");
                                }
                            |   PrimaryExpression 
                                {
                                    emit("(StoreArgAcc " + 
                                        --temporaryActivationRecord + " 0)");
                                }
                                OPERATOR Expression
                                { 
                                    emit("(StoreArgAcc " + 
                                        temporaryActivationRecord + " 1)");
                                    emit("(Call #\"" + $3 + "[f2]\" " + 
                                        temporaryActivationRecord++ + ")");
                                }
                            ;
PrimaryExpression           :   ParenExpression
                            |   NAME 
                                {
                                    emit("(Fetch " + findVar($1)+")");
                                }
                            |   LITERAL 
                                {
                                    emit("(MakeVal " + $1 + ")");
                                }
                            |   FunctionCall
                            ;
FunctionCall                :   NAME, '(' 
                                {
                                    --temporaryActivationRecord;
                                    push(variablePosition);
                                    variablePosition = 0;
                                }
                                ParametersOpt ')'
                                {
                                    emit("(Call #\"" + $1 + "[f" + 
                                        variablePosition
                                        + "]\" " + 
                                        temporaryActivationRecord++ + ")");
                                        variablePosition = pop();
                                }
                                ;
Parameters                  :   Expression 
                                {
                                    emit("(StoreArgAcc " + 
                                        temporaryActivationRecord + " " + 
                                        variablePosition++ + ")");
                                }
                                ParametersExtension;
ParametersExtension         :   /* Nothing */
                            |   ',' Parameters
                            ;
ParametersOpt               :   /* Nothing */
                            |   Parameters
                            ;
ParenExpression             :   '(' Expression ')';
FlowBody                    :   '{' Statements '}';
%%
static private String name;
private Yylex lexer;
private HashMap<String,Integer> varTable;
private int varCount;
private int temporaryActivationRecord = -1;
private int variablePosition = 0;
private int nextLabel = 1;
private static class Link
{
    int value;
    Link next;

    public Link(int value, Link next_link)
    {
        this.value = value;
        this.next = next_link;
    }
}
Link stack = null;

private int newLabel()
{
    return nextLabel++;
}

private void push(int value)
{
    stack = new Link(value, stack);
}

private int pop()
{
    int result = stack.value;
    stack = stack.next;
    return result;
}

private int yylex()
{
    int yyl_return = -1;
    try
    {
        yylval = new ParserVal(0);
        yyl_return = lexer.yylex();
    }
    catch (IOException e)
    {
        System.err.println("IO error: " + e);
    }
    return yyl_return;
}

public void yyerror(String error)
{
    System.err.println("Error: " + error + " in line " + (lexer.getLine()+1) + 
        " and column " + (lexer.getColumn()+1));
}

public Parser(Reader r)
{
    lexer = new Yylex(r, this);
}

public void emit(String in)
{
    System.out.println(in);
}

public void addVar(String name)
{
    if(varTable.get(name) != null)
    {
        yyerror("Variable " + name + " already exists");
    }
    varTable.put(name,varCount++);
}

public int findVar(String name)
{
    Integer variable_position = varTable.get(name);
    if(variable_position == null)
        yyerror("Variable " + name + " does not exist");
    return variable_position;
}

// Notkun: emitLogicOperators()
// Eftir:  Búið er að skrifa út smálamálsþulu fyrir rökvirkjana ,||', ,&&' og 
//              ,!'.
public void emitLogicOperators()
{
    int andLabel = newLabel();
    int orLabel = newLabel();
    String and = "#\"&&[f2]\" =" +
                    "[" +
                    "(Fetch 0)" +
                    "(GoFalse _" + andLabel + ")" +
                    "(Fetch 1)" +
                    "(Return)" +
                    "_" + andLabel + ":" +
                    "(Return)" +
                    "];";

    String or = "#\"||[f2]\" =" +
                    "[" +
                    "(Fetch 0)" +
                    "(GoTrue _" + orLabel + ")" +
                    "(Fetch 1)" +
                    "(Return)" +
                    "_" + orLabel +":" +
                    "(Return)" +
                    "];";

    String not = "#\"![f1]\" =" +
                    "[" +
                    "(Fetch 0)" +
                    "(Not)" +
                    "(Return)" +
                    "];";

    System.out.println(and + "\n" + or + "\n" + not);
}

public static void main(String args[]) throws IOException
{

    Parser yyparser;
    if ( args.length > 0 ) {
        // parse a file
        name = args[0].substring(0,args[0].lastIndexOf('.'));
        yyparser = new Parser(new FileReader(args[0]));
    }
    else {
        // interactive mode
        System.out.println("Þáttari fyrir forritunarmálið Blæ.");
        System.out.println("Vinsamlegast skrifið nafn á einingu: ");
        Scanner scan = new Scanner(System.in);
        name = scan.nextLine();
        System.out.print("Input: ");
	    yyparser = new Parser(new InputStreamReader(System.in));
    }

    yyparser.yyparse();
}
