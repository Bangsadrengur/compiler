/*
 *  Höfundur: Heimir Þór Kjartansson
 *  
 *  Þessari lesgreinauppskrift má varpa í fullvirkan lesgreini með JFlex á 
        eftirfarandi hátt
 *      jflex lexer.flex
 *  ef gert er ráð fyrir að jflex sé skipun á kerfi notanda.
 *  Þessi lesgreinir er hannaður til að tala við þáttara fengnum í gegnum 
        Byacc/j þáttarasmið.
 *  Gert er ráð fyrir að Byacc/j þáttarasmiðurinn búi til Java klasa Parser 
        sem framkallaði
 *      lesgreinirinn getur talað við.
 *  Framkallaði lesgreinirinn er ekki starfhæfur einn og sér.
 */

%%

%public
%byaccj
%unicode
%line
%column

%{
    public int getLine() {return yyline;}
    public int getColumn() {return yycolumn;}

    private Parser yyparser;

    public Yylex(java.io.Reader r, Parser yyparser)
    {
        this(r);
        this.yyparser = yyparser;
    }
%}

    /* Reglulegar skilgreiningar */

    TS      = [0-9]
    DIGITS  = [0-9]+
    WS      = [ \t\n\r]
    PF      = [+\-]
    OP      = [%/?~\^:<>!&|*/]
    OPS     = ({OP}|{PF})+
    EQ      = [=]
    EQOPS   = (({EQ}{OPS})|({OPS}{EQ})|{EQ}({EQ})+)+
    NS      = [a-zA-ZþæöðáéýúíóÞÆÖÐÁÉÝÚÍÓ]
    DELIM   = [()\[\]{}.,;]
    // OP er virki.
    // OPS TODO
    // EQ TODO
    // WS er bilstafur
    // NS er bókstafur
    // TS er tölustafur
    // PF er reiknivirki sem einnig gæti verið formerki
    // DELIM eru aðgreiningartákn

%%

if                                          {return yyparser.IF;}
else                                        {return yyparser.ELSE;}
while                                       {return yyparser.WHILE;}
{WS}                                        
{
    /* WS flokkast sem athugasemd - engu skilað */
}
"//".*$                                     
{
    /* C athugasemdastíll, skilar engu.*/
}
{DELIM}                                     {return (int) yycharat(0);}
"var"                                       {return yyparser.VAR;}
"true"                                      
{
    yyparser.yylval = new ParserVal(yytext()); return yyparser.LITERAL;
}
"false"                                  
{
    yyparser.yylval = new ParserVal(yytext()); return yyparser.LITERAL;
}
"null"                                    
{
    yyparser.yylval = new ParserVal(yytext()); return yyparser.LITERAL;
}
"\"".*"\""                               
{
    yyparser.yylval = new ParserVal(yytext()); return yyparser.LITERAL;
}
"return"                                    {return yyparser.RETURN;}
("_"|{NS})("_"|{NS}|{TS})*                  
{
    yyparser.yylval = new ParserVal(yytext()); return yyparser.NAME;
}
{EQOPS}|{OPS}                               
{
    yyparser.yylval = new ParserVal(yytext()); return yyparser.OPERATOR;
}
{EQ}                                        {return (int) yycharat(0);}
{DIGITS}
{
    yyparser.yylval = new ParserVal(yytext()); return yyparser.LITERAL;
}
.  
{ 
    System.err.println("Error: unexpected character '"+yytext()+"'" + 
    " in line " + (yyline+1) + " and column " + (yycolumn+1)); 
    return Parser.YYERRCODE; 
}                         
