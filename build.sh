#!/bin/sh
rm -r run production;
mkdir production;
cp ./lexer/lexer.flex production;
cp ./parser/parser.y production;
jflex ./production/lexer.flex;
byaccj -J ./production/parser.y;
mkdir run;
javac -d ./run ./production/*.java;
jar cfm Blaer.jar Manifest.txt -C run .
rm -r production;
rm *.java;
