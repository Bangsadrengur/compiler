Forritunarmálið ,Blær'
===============
Um verkefni
---------------
*Höfundur:* Heimir Þór Kjartansson

*Gert fyrir:* Áfangann ,Þýðendur TÖL202M' vorið 2013.

Hlutar verkefnis
------------------------------------
### Mállýsing (./grammar/blaerMallys.ebnf)
EBNF mállýsing fyrir forritunarmálið Blær.

Þessi mállýsing er unnin með hliðsjón af mállýsingum í forritunarmálinu Morpho
    (morpho.cs.hi.is) og nánast (e. not quite) LL(1) mállýsingu sem hefur verið gerð fyrir Javascript 
    (http://hepunx.rl.ac.uk/~adye/jsspec11/llr.htm).

Nefnanlegar hönnunarákvarðanir í máli:

* Forrit hefur lágmark eitt fall.
* Forrit er runa af föllum
* Föll skila ávallt síðasta úrunna gildi.
* Leppar í föll eru segðir án gildunar.
* Breytur eru aðeins í einni földunarhæð, þ.e. inni í falli.
* Breytur eru aðeins tilteknar á einum stað í falli og það fremst.
    - Þessu má mögulega hnika á einfaldan hátt með að færa 
        breytutilgreiningar undir setningar.
* Enginn forgangur aðgerða.
* Setningar sem enda á semíkommu eru allar setningar utan 
    flæðisstjórnarsetninga.
* Stýrisegðir flæðisstjórnunarsetninga krefjast ekki umlykjandi sviga.

### Lesgreinir (./lexer/lexer.flex)
Þessari lesgreinauppskrift má varpa í fullvirkan lesgreini með JFlex á eftirfarandi hátt

>    jflex lexer.flex

ef gert er ráð fyrir að jflex sé skipun á kerfi notanda.

Þessi lesgreinir er hannaður til að tala við þáttara fengnum í gegnum BYACC/J þáttarasmið.

Gert er ráð fyrir að BYACC/J þáttarasmiðurinn búi til Java klasa Parser sem framkallaði
    lesgreinirinn getur talað við.

Framkallaði lesgreinirinn er ekki starfhæfur einn og sér.

### Þáttari (./parser/parser.y)
Þessari þáttarauppskrift má varpa í þáttara með BYACC/J á eftirfarandi hátt

>    byaccj -J parser.y

ef gert er ráð fyrir að byaccj sé skipun á kerfi notanda.

Þessi þáttari er hannaður til að virka með lesgreini fengnum í gegnum JFlex lesgreinasmið.

Gert er ráð fyrir að JFlex lesgreinasmiðurinn búi til Java klasa Yylex sem framkallaði þáttarinn
    getur talað við.

Framkallaði þáttarinn er starfhæfur með fyrrnefndum lesgreini.

Eiginleikar þáttara:

* Tiltekur staðsetningu fundinna villna.
* Hefur bæði gagnvirkt viðmót og les inn heilar skrár.
* Skrifað er á aðalúttak öll gildi sem skilast úr fallsköllum svo hægt einfalt sé að rekja virkni þáttara.

Forkröfur þáttara:

* Forrit þarf að hafa eitt fall main() sem meginfall án leppa.

### Hvernig skal keyra kerfið og samantekt þar á
Framkallið lesgreini:

> jflex lexer.flex

Framkallið þáttara:

> byaccj -J parser.y

Þýðið fengnar skrár með Java:

> javac Yylex.java Parser.java ParserVal.java

þar sem Yylex.java er fengin skrá úr jflex og Parser.java og ParserVal.java frá BYACC/J.

Keyrið þáttara og lesgreini með Java í gagnvirku viðmóti:

> java Parser

eða keyrið þáttara og lesgreini með Java fyrir tiltekna hreintexta skrá með kóða fyrir Blæ:

> java Parser skrá

þar sem ,skrá' er slóð á skrá til að þátta.

### Umfram skrár í gagnahirslu
Í gagnahirslu eru umfram skráa fyrir þýðandann (s.s. jflex, byacc og 
mállýsingaskráa) ýmsar aðrar skrár sem tengjast verkefni. Þessar skrár eru
sem hér fer.

* Test.blaer
* Test.sh
* build.sh
* all.sh

Ein prufuskrá (Test.blaer) er til staðar. Innihald hennar er ekki lýsandi fyrir
heildarvirkni kerfis en frekar fyrir þann eiginleika þýðanda sem var síðast
útfærður.

Þrjú Bash handrit (Test.sh, build.sh, all.sh) eru í gagnahirslu. Sé fullvirkt Bash umhverfi fyrir hendi má 
byggja starfhæfa útgáfu þýðanda með að kalla á eftirfarandi.

> ./build.sh

Þá má keyra prufuskrá (Test.blaer) með eftirfarandi skipun ef Morpho þýðandi
er uppsettur hjá notanda og leið (e. path) uppsett umhverfi.

> ./Test.sh

Niðurstaða keyrslu prufuskrár er þýðing á stefi í skránni Test.blaer og keyrsla
á útkomunni.
þá verður til mappan ./run/ með keyrsluskrám fyrir þýðanda og skrárnar 
Test.mexe og Test.morpho. Test.morpho inniheldur Morpho smalamál sem er þýðing
á stefi í Test.blaer. Test.mexe er keyrsluskrá fyrir Morpho, þýtt frá stefinu
í skránni Test.blaer.  

Setja má upp allt kerfið og keyra prófanir í einni skipan með eftirfarandi
skipun. 

> ./all.sh

Athugið að handritið gerir ekkert frekar en að keyra build.sh og 
Test.sh.


Hlekkir á notuð verkfæri/forrit
--------------------------------
### Sértækt við forritunarmálið Blæ
Forritunarmálið Morpho.
<http://morpho.cs.hi.is/>

Lesgreinasmiðurinn JFlex.
<http://www.jflex.de/>

Þáttarasmiðurinn BYACC/J.
<http://byaccj.sourceforge.net/>

### Stuðst við af sértækum verkfærum eða til að einfalda uppsetningu
Forritunarmálið Java
<http://www.java.com/>

Unix skelin Bash
<http://www.gnu.org/software/bash/>
### Ónauðsynlegt en notað óspart
Ritillinn vim
<http://www.vim.org/>
